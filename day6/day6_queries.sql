-- in natural language mode
SELECT 
title, 
author 
FROM catalog 
WHERE MATCH(title, author) 
AGAINST('king carrie' IN NATURAL LANGUAGE MODE);

-- in boolean mode
select 
title, 
author 
from catalog 
WHERE MATCH(title, author) 
AGAINST('king -carrie\-jones' IN BOOLEAN MODE);

SELECT 
book.title,
author.name
FROM book
JOIN author USING(author_id)
WHERE MATCH(book.title)
OR 
MATCH(author.name)
AGAINST('king carrie');

-- CREATE VIEW
CREATE VIEW book_view
AS 
SELECT
book.title,
author.name as author,
publisher.name as publisher,
format.name as format,
genre.name as genre
FROM
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
JOIN genre USING(genre_id);