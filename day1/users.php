<?php

$dbh = new PDO('sqlite:database.sqlite');

$query = 'SELECT id,name,email,phone FROM users';

$stmt = $dbh -> query($query);
$results = $stmt -> fetchALL(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html>
<head>
	<title>Users Info</title>
	<style>
		table{
			border: 1px solid #000;
			border-collapse: collapse;
			margin-bottom: 20px;
		}
		td,th{
			padding: 6px;
			border: 1px solid #000;
		}
		th{
			background-color: #ccc;
		}
	</style>
</head>
<body>
	<table>
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>EMAIL</th>
			<th>PHONE</th>
		</tr>
		<?php
		foreach($results as $row) : ?>
			<tr>
				<td><?=$row['id']?></td>
				<td><?=$row['name']?></td>
				<td><?=$row['email']?></td>
				<td><?=$row['phone']?></td>
			</tr>
		<?php endforeach; ?>
	</table>
	<table>
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>EMAIL</th>
			<th>PHONE</th>
		</tr>
		<?php
		foreach($results as $row) : ?>
			<tr>
				<?php foreach($row as $key => $value) : ?>
					<td><?=$value?></td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>