-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Gaming_DB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Gaming_DB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Gaming_DB` DEFAULT CHARACTER SET utf8 ;
USE `Gaming_DB` ;

-- -----------------------------------------------------
-- Table `Gaming_DB`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Gaming_DB`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(200) NULL,
  `phone` VARCHAR(20) NULL,
  `address_line1` VARCHAR(100) NULL,
  `city` VARCHAR(50) NULL,
  `province` VARCHAR(50) NULL,
  `postal_code` VARCHAR(8) NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Gaming_DB`.`invoice`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Gaming_DB`.`invoice` (
  `invoice_id` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NOT NULL,
  `bill_to` VARCHAR(400) NULL,
  `ship_to` VARCHAR(400) NULL,
  `date` DATE NULL,
  `due_date` DATE NULL,
  `details` VARCHAR(600) NULL,
  `total` FLOAT NULL COMMENT '\n',
  PRIMARY KEY (`invoice_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Gaming_DB`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Gaming_DB`.`products` (
  `product_id` INT NOT NULL AUTO_INCREMENT,
  `product_sku` INT(8) NOT NULL,
  `name` VARCHAR(255) NULL,
  `platform` VARCHAR(255) NULL,
  `publisher` VARCHAR(255) NULL,
  `developer` VARCHAR(255) NULL,
  `price` FLOAT NULL,
  `genre` VARCHAR(255) NULL,
  `category` VARCHAR(255) NULL,
  `images` VARCHAR(255) NULL,
  `thumbnail` VARCHAR(255) NULL,
  `description` LONGTEXT NULL,
  `rating` TEXT NULL,
  `supplier` VARCHAR(255) NULL,
  `quantity` INT NULL,
  `in_stock` TINYINT(1) NULL,
  `user_id` INT NULL,
  `invoice_id` INT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  INDEX `user_id_idx` (`user_id` ASC),
  INDEX `invoice_id_idx` (`invoice_id` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `Gaming_DB`.`user` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `invoice_id`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `Gaming_DB`.`invoice` (`invoice_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `products`
(
 `product_sku`,
 `name`,
 `platform`,
 `publisher`,
 `developer`,
 `price`, 
 `genre`, 
 `category`,
 `images`, 
 `thumbnail`,
 `description`,
 `rating`,
 `supplier`,
 `quantity`,
 `in_stock`
)
VALUES
(
 '18001110',
 'GTA5',
 'PC,PS4,Xbox One',
 'Rockstar Games',
 'Rockstar North',
 '30.49',
 'Action,Adventure',
 'Third Person Action, Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\gta5.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\gta5.jpg"',
 'Grand Theft Auto V is an action-adventure video game developed by Rockstar North and published by Rockstar Games.',
 'Mature',
 'vipoutletcanada',
 40,
 1
),
(
 '18000100',
 'Uncharted 4',
 'PS4',
 'Sony Interactive Entertainment',
 'Naughty Dog',
 '51.49',
 'Action,Adventure',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\uncharted4.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\uncharted4.jpg"',
 'Uncharted 4: A Thiefs End is an action-adventure game developed by Naughty Dog and published by Sony Computer Entertainment for PlayStation 4 in May 2016',
 'Mature',
 'vipoutletcanada',
 90,
 1
),
(
 '18000200',
 'God of War',
 'PS4,PS3',
 'Sony Interactive Entertainment',
 'SIE Santa Monica',
 '75.00',
 'Action,Adventure,Hack and Slash',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\godofwar.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\godofwar.jpg"',
 'God of War is a mythology-based action-adventure hack and slash video game franchise.',
 'Mature, 18+',
 'millercanada',
 120,
 1
),
(
 '18000300',
 'Halo 4',
 'Xbox 360, Xbox One',
 'Microsoft Studios',
 '343 Industries',
 '29.94',
 'Shooting',
 'First Player Shooter, Single Player',
 '"H:/\WDD winnipeg/\images/\halo4.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\halo4.jpg"',
 'Halo 4 is a first-person shooter video game developed by 343 Industries and published by Microsoft Studios for the Xbox 360 video game console. ',
 'Mature, 18+',
 'topgames',
 20,
 1
),
(
 '18000400',
 'Need for Speed Payback',
 'PC,PS4,Xbox One',
 'Electronic Arts',
 'Ghost Games',
 '34.94',
 'Racing',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\payback.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\payback.jpg"',
 'Need for Speed Payback is a racing video game developed by Ghost Games and published by Electronic Arts for Microsoft Windows, PlayStation 4 and Xbox One.',
 '7+',
 'jackstores',
 80,
 1
),
(
 '18000500',
 'For Honor',
 'PC,PS4,Xbox One',
 'Ubisoft',
 'Ubisoft Montreal',
 '26.99',
 'Action, Fighter, Hack and Slash',
 'Single Player,Third Person Action',
 '"H:/\WDD winnipeg/\images/\forhonor.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\forhonor.jpg"',
 'For Honor is a video game developed and published by Ubisoft for Microsoft Windows, PlayStation 4, and Xbox One.',
 '1Mature,8+',
 'gamehunt',
 60,
 1
),
(
 '18000600',
 'Assassins Creed Origins',
 'PC,PS4,Xbox One',
 'Ubisoft',
 'Ubisoft Montreal',
 '52.94',
 'Action, Adventure, Stleath',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\origins.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\origins.jpg"',
 'Assassins Creed Origins is an action-adventure video game developed by Ubisoft Montreal and published by Ubisoft.',
 'Mature, 18+',
 'vipoutletcanada',
 30,
 1
),
(
 '18000800',
 'NBA 2K17',
 'PC,PS4,Xbox One, PS3, Xbox 360',
 '2K Sports',
 'Visual Concepts',
 '43.33',
 'Sports',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\nba.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\nba.jpg"',
 'NBA 2K17 is a basketball simulation video game developed by Visual Concepts and published by 2K Sports.',
 '10+',
 'gamehunt',
 70,
 1
),
(
 '18000900',
 'WWE 2K17',
 'PC,PS4,Xbox One, PS3, Xbox 360',
 '2K Games, 2K Sports',
 'Yuke Visual Concepts',
 '49.95',
 'Sports',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\wwe17.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb\wwe17.jpg"',
 'WWE 2K17 is a professional wrestling video game developed in a collaboration between Yukes and Visual Concepts, and published by 2K Sports for PlayStation 3, PlayStation 4, Xbox 360, Xbox One, and Microsoft Windows.',
 '10+',
 'gamehunt',
 70,
 1
),
(
 '18000110',
 'WWE 2K18',
 'PC,PS4,Xbox One',
 '2K Games, 2K Sports',
 'Yuke Visual Concepts',
 '59.95',
 'Sports',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\wwe18.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\wwe18.jpg"',
 'WWE 2K18 is a professional wrestling video game developed in a collaboration between Yukes and Visual Concepts, and published by 2K Sports for PlayStation 3, PlayStation 4, Xbox 360, Xbox One, and Microsoft Windows.',
 '10+',
 'gamehunt',
 70,
 1
),
(
 '18000120',
 'Far Cry 5',
 'PC,PS4,Xbox One',
 'Ubisoft',
 'Ubisoft Montreal, Ubisoft Toronto',
 '55.89',
 'Action, Adventure, First Person Shooting',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\farcry5.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\farcry5.jpg"',
 'Far Cry 5 is an action-adventure first-person shooter video game developed by Ubisoft Montreal and Ubisoft Toronto and published by Ubisoft for Microsoft Windows, PlayStation 4 and Xbox One.',
 'Mature, 18+',
 'vipoutletcanada',
 100,
 1
),
(
 '18000130',
 'Gears of War 4',
 'PC,Xbox One',
 'Microsoft Studios',
 'The Coalition',
 '43.89',
 'First Person Shooting',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\farcry5.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\farcry5.jpg"',
 'Far Cry 5 is an action-adventure first-person shooter video game developed by Ubisoft Montreal and Ubisoft Toronto and published by Ubisoft for Microsoft Windows, PlayStation 4 and Xbox One.',
 'Mature, 18+',
 'vipoutletcanada',
 20,
 1
)
;

INSERT INTO `products`
(
 `product_sku`,
 `name`,
 `platform`,
 `publisher`,
 `developer`, 
 `genre`, 
 `category`,
 `images`, 
 `thumbnail`,
 `description`,
 `rating`,
 `in_stock`
)
VALUES(
 '19001000',
 'Anthem',
 'PC, PS4, Xbox One',
 'Electronic Arts',
 'BioWare',
 'Action, Role Playing',
 'Third Person Shooter, Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\anthem.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\anthem.jpg"',
 'Anthem is an upcoming online multiplayer action role-playing video game being developed by BioWare and published by Electronic Arts. The game is slated for a worldwide release on February 22, 2019, for Microsoft Windows, PlayStation 4 and Xbox One.',
 'Rating Pending',
 0
),
(
 '19002000',
 'Tomb Raider Shadow of Tomb Raider',
 'PC, PS4, Xbox One',
 'Square Enix Co.',
 'Crystal Dynamics',
 'Action, Adventure',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\tombraider.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\tombraider.jpg"',
 'Shadow of the Tomb Raider is an upcoming action-adventure video game developed by Eidos Montréal in conjunction with Crystal Dynamics and published by Square Enix.',
 'Rating Pending',
 0
),
(
 '19003000',
 'The Last of Us 2',
 'PS4',
 'Sony Interactive Entertainment',
 'Naughty Dog',
 'Action, Adventure, Survival Horror',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\lastofus.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\lastofus.jpg"',
 'The Last of Us Part II is an upcoming action-adventure survival horror video game developed by Naughty Dog and published by Sony Interactive Entertainment for PlayStation 4.',
 'Rating Pending',
 0
),
(
 '19004000',
 'Spiderman 2018',
 'PS4',
 'Sony Interactive Entertainment',
 'Insomaniac Games',
 'Action, Adventure',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\spiderman.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\spiderman.jpg"',
 'Marvels Spider-Man is an upcoming action-adventure game based on the Marvel Comics superhero Spider-Man, developed by Insomniac Games and published by Sony Interactive Entertainment for PlayStation 4. ',
 'Rating Pending',
 0
),
(
 '19005000',
 'FIFA 19',
 'PC, PS4, PS3, Xbox 360, Xbox One',
 'EA Sports',
 'EA Canada',
 'Sports',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\fifa.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\fifa.jpg"',
 'FIFA 19 is an upcoming football simulation video game developed by EA Canada, as part of Electronic Arts FIFA series.',
 'Rating Pending',
 0
),
(
 '19006000',
 'Assassins Creed Odyssey',
 'PC, PS4, Xbox One',
 'Ubisoft',
 'Ubisoft Ukraine, Ubisoft Montreal',
 'Action, Adventure, Stleath',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\odyssey.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\odyssey.jpg"',
 'Assassins Creed Odyssey is an upcoming action role-playing video game developed by Ubisoft Quebec and published by Ubisoft.',
 'Rating Pending',
 0
),
(
 '19007000',
 'Call of Duty Black Ops 4',
 'PC, PS4, Xbox One',
 'Activision',
 'Treyarch',
 'Third Person Shooter, Battle Royale',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\blackops.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\blackops.jpg"',
 'Call of Duty: Black Ops 4 is an upcoming multiplayer first-person shooter developed by Treyarch and published by Activision. ',
 'Rating Pending',
 0
),
(
 '19008000',
 'Strange Brigade',
 'PC, PS4, Xbox One',
 'Rebellion Developments',
 'Rebellion Developments',
 'Third Person Shooter',
 'Single Player, Multiplayer',
 '"H:/\WDD winnipeg/\images/\strangebrigade.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\strangebrigade.jpg"',
 'Strange Brigade is an upcoming cooperative third-person shooter video game developed and published Rebellion Developments.',
 'Rating Pending',
 0
),
(
 '19009000',
 'Ghost of tsushima',
 'PS4',
 'Sony Interactive Entertainment',
 'Sucker Punch Productions',
 'Action, Adventure, Stleath',
 'Single Player',
 '"H:/\WDD winnipeg/\images/\tsushima.jpg"',
 '"H:/\WDD winnipeg/\images/\thumb/\tsushima.jpg"',
 'Ghost of Tsushima is an upcoming action-adventure video game developed by Sucker Punch Productions and published by Sony Interactive Entertainment for the PlayStation 4',
 'Rating Pending',
 0
);