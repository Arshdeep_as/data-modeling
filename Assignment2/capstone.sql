-- MySQL dump 10.16  Distrib 10.2.12-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: Gaming_DB
-- ------------------------------------------------------
-- Server version	10.2.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(45) NOT NULL,
  `bill_to` varchar(400) DEFAULT NULL,
  `ship_to` varchar(400) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `details` varchar(600) DEFAULT NULL,
  `total` float DEFAULT NULL COMMENT '\n',
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_sku` int(8) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `developer` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `rating` text DEFAULT NULL,
  `supplier` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(1) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `invoice_id_idx` (`invoice_id`),
  CONSTRAINT `invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,18001110,'GTA5','PC,PS4,Xbox One','Rockstar Games','Rockstar North',30.49,'Action,Adventure','Third Person Action, Single Player, Multiplayer','\"H:/WDD winnipeg/images/gta5.jpg\"','\"H:/WDD winnipeg/images/	humb/gta5.jpg\"','Grand Theft Auto V is an action-adventure video game developed by Rockstar North and published by Rockstar Games.','Mature','vipoutletcanada',40,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(2,18000100,'Uncharted 4','PS4','Sony Interactive Entertainment','Naughty Dog',51.49,'Action,Adventure','Single Player','\"H:/WDD winnipeg/images/uncharted4.jpg\"','\"H:/WDD winnipeg/images/	humb/uncharted4.jpg\"','Uncharted 4: A Thiefs End is an action-adventure game developed by Naughty Dog and published by Sony Computer Entertainment for PlayStation 4 in May 2016','Mature','vipoutletcanada',90,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(3,18000200,'God of War','PS4,PS3','Sony Interactive Entertainment','SIE Santa Monica',75,'Action,Adventure,Hack and Slash','Single Player','\"H:/WDD winnipeg/images/godofwar.jpg\"','\"H:/WDD winnipeg/images/	humb/godofwar.jpg\"','God of War is a mythology-based action-adventure hack and slash video game franchise.','Mature, 18+','millercanada',120,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(4,18000300,'Halo 4','Xbox 360, Xbox One','Microsoft Studios','343 Industries',29.94,'Shooting','First Player Shooter, Single Player','\"H:/WDD winnipeg/images/halo4.jpg\"','\"H:/WDD winnipeg/images/	humb/halo4.jpg\"','Halo 4 is a first-person shooter video game developed by 343 Industries and published by Microsoft Studios for the Xbox 360 video game console. ','Mature, 18+','topgames',20,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(5,18000400,'Need for Speed Payback','PC,PS4,Xbox One','Electronic Arts','Ghost Games',34.94,'Racing','Single Player, Multiplayer','\"H:/WDD winnipeg/images/payback.jpg\"','\"H:/WDD winnipeg/images/	humb/payback.jpg\"','Need for Speed Payback is a racing video game developed by Ghost Games and published by Electronic Arts for Microsoft Windows, PlayStation 4 and Xbox One.','7+','jackstores',80,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(6,18000500,'For Honor','PC,PS4,Xbox One','Ubisoft','Ubisoft Montreal',26.99,'Action, Fighter, Hack and Slash','Single Player,Third Person Action','\"H:/WDD winnipeg/images/forhonor.jpg\"','\"H:/WDD winnipeg/images/	humb/forhonor.jpg\"','For Honor is a video game developed and published by Ubisoft for Microsoft Windows, PlayStation 4, and Xbox One.','1Mature,8+','gamehunt',60,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(7,18000600,'Assassins Creed Origins','PC,PS4,Xbox One','Ubisoft','Ubisoft Montreal',52.94,'Action, Adventure, Stleath','Single Player','\"H:/WDD winnipeg/images/origins.jpg\"','\"H:/WDD winnipeg/images/	humb/origins.jpg\"','Assassins Creed Origins is an action-adventure video game developed by Ubisoft Montreal and published by Ubisoft.','Mature, 18+','vipoutletcanada',30,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(8,18000800,'NBA 2K17','PC,PS4,Xbox One, PS3, Xbox 360','2K Sports','Visual Concepts',43.33,'Sports','Single Player, Multiplayer','\"H:/WDD winnipeg/images/\nba.jpg\"','\"H:/WDD winnipeg/images/	humb/\nba.jpg\"','NBA 2K17 is a basketball simulation video game developed by Visual Concepts and published by 2K Sports.','10+','gamehunt',70,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(9,18000900,'WWE 2K17','PC,PS4,Xbox One, PS3, Xbox 360','2K Games, 2K Sports','Yuke Visual Concepts',49.95,'Sports','Single Player, Multiplayer','\"H:/WDD winnipeg/images/wwe17.jpg\"','\"H:/WDD winnipeg/images/	humbwwe17.jpg\"','WWE 2K17 is a professional wrestling video game developed in a collaboration between Yukes and Visual Concepts, and published by 2K Sports for PlayStation 3, PlayStation 4, Xbox 360, Xbox One, and Microsoft Windows.','10+','gamehunt',70,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(10,18000110,'WWE 2K18','PC,PS4,Xbox One','2K Games, 2K Sports','Yuke Visual Concepts',59.95,'Sports','Single Player, Multiplayer','\"H:/WDD winnipeg/images/wwe18.jpg\"','\"H:/WDD winnipeg/images/	humb/wwe18.jpg\"','WWE 2K18 is a professional wrestling video game developed in a collaboration between Yukes and Visual Concepts, and published by 2K Sports for PlayStation 3, PlayStation 4, Xbox 360, Xbox One, and Microsoft Windows.','10+','gamehunt',70,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(11,18000120,'Far Cry 5','PC,PS4,Xbox One','Ubisoft','Ubisoft Montreal, Ubisoft Toronto',55.89,'Action, Adventure, First Person Shooting','Single Player, Multiplayer','\"H:/WDD winnipeg/images/farcry5.jpg\"','\"H:/WDD winnipeg/images/	humb/farcry5.jpg\"','Far Cry 5 is an action-adventure first-person shooter video game developed by Ubisoft Montreal and Ubisoft Toronto and published by Ubisoft for Microsoft Windows, PlayStation 4 and Xbox One.','Mature, 18+','vipoutletcanada',100,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(12,18000130,'Gears of War 4','PC,Xbox One','Microsoft Studios','The Coalition',43.89,'First Person Shooting','Single Player','\"H:/WDD winnipeg/images/farcry5.jpg\"','\"H:/WDD winnipeg/images/	humb/farcry5.jpg\"','Far Cry 5 is an action-adventure first-person shooter video game developed by Ubisoft Montreal and Ubisoft Toronto and published by Ubisoft for Microsoft Windows, PlayStation 4 and Xbox One.','Mature, 18+','vipoutletcanada',20,1,NULL,NULL,'2018-07-22 15:41:02',NULL),(13,19001000,'Anthem','PC, PS4, Xbox One','Electronic Arts','BioWare',NULL,'Action, Role Playing','Third Person Shooter, Single Player, Multiplayer','\"H:/WDD winnipeg/images/anthem.jpg\"','\"H:/WDD winnipeg/images/	humb/anthem.jpg\"','Anthem is an upcoming online multiplayer action role-playing video game being developed by BioWare and published by Electronic Arts. The game is slated for a worldwide release on February 22, 2019, for Microsoft Windows, PlayStation 4 and Xbox One.','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(14,19002000,'Tomb Raider Shadow of Tomb Raider','PC, PS4, Xbox One','Square Enix Co.','Crystal Dynamics',NULL,'Action, Adventure','Single Player','\"H:/WDD winnipeg/images/	ombraider.jpg\"','\"H:/WDD winnipeg/images/	humb/	ombraider.jpg\"','Shadow of the Tomb Raider is an upcoming action-adventure video game developed by Eidos Montréal in conjunction with Crystal Dynamics and published by Square Enix.','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(15,19003000,'The Last of Us 2','PS4','Sony Interactive Entertainment','Naughty Dog',NULL,'Action, Adventure, Survival Horror','Single Player, Multiplayer','\"H:/WDD winnipeg/images/lastofus.jpg\"','\"H:/WDD winnipeg/images/	humb/lastofus.jpg\"','The Last of Us Part II is an upcoming action-adventure survival horror video game developed by Naughty Dog and published by Sony Interactive Entertainment for PlayStation 4.','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(16,19004000,'Spiderman 2018','PS4','Sony Interactive Entertainment','Insomaniac Games',NULL,'Action, Adventure','Single Player','\"H:/WDD winnipeg/images/spiderman.jpg\"','\"H:/WDD winnipeg/images/	humb/spiderman.jpg\"','Marvels Spider-Man is an upcoming action-adventure game based on the Marvel Comics superhero Spider-Man, developed by Insomniac Games and published by Sony Interactive Entertainment for PlayStation 4. ','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(17,19005000,'FIFA 19','PC, PS4, PS3, Xbox 360, Xbox One','EA Sports','EA Canada',NULL,'Sports','Single Player, Multiplayer','\"H:/WDD winnipeg/images/fifa.jpg\"','\"H:/WDD winnipeg/images/	humb/fifa.jpg\"','FIFA 19 is an upcoming football simulation video game developed by EA Canada, as part of Electronic Arts FIFA series.','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(18,19006000,'Assassins Creed Odyssey','PC, PS4, Xbox One','Ubisoft','Ubisoft Ukraine, Ubisoft Montreal',NULL,'Action, Adventure, Stleath','Single Player','\"H:/WDD winnipeg/images/odyssey.jpg\"','\"H:/WDD winnipeg/images/	humb/odyssey.jpg\"','Assassins Creed Odyssey is an upcoming action role-playing video game developed by Ubisoft Quebec and published by Ubisoft.','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(19,19007000,'Call of Duty Black Ops 4','PC, PS4, Xbox One','Activision','Treyarch',NULL,'Third Person Shooter, Battle Royale','Single Player, Multiplayer','\"H:/WDD winnipeg/images/lackops.jpg\"','\"H:/WDD winnipeg/images/	humb/lackops.jpg\"','Call of Duty: Black Ops 4 is an upcoming multiplayer first-person shooter developed by Treyarch and published by Activision. ','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(20,19008000,'Strange Brigade','PC, PS4, Xbox One','Rebellion Developments','Rebellion Developments',NULL,'Third Person Shooter','Single Player, Multiplayer','\"H:/WDD winnipeg/images/strangebrigade.jpg\"','\"H:/WDD winnipeg/images/	humb/strangebrigade.jpg\"','Strange Brigade is an upcoming cooperative third-person shooter video game developed and published Rebellion Developments.','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL),(21,19009000,'Ghost of tsushima','PS4','Sony Interactive Entertainment','Sucker Punch Productions',NULL,'Action, Adventure, Stleath','Single Player','\"H:/WDD winnipeg/images/	sushima.jpg\"','\"H:/WDD winnipeg/images/	humb/	sushima.jpg\"','Ghost of Tsushima is an upcoming action-adventure video game developed by Sucker Punch Productions and published by Sony Interactive Entertainment for the PlayStation 4','Rating Pending',NULL,NULL,0,NULL,NULL,'2018-07-22 15:41:04',NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address_line1` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-22 15:41:52
