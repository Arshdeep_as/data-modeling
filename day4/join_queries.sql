 
 select
 book.title,
 genre.name as genre 
 from 
 book 
 join genre using(genre_id);

 select
 book.title,
 genre.name as genre 
 from 
 book, genre
 WHERE
 book.genre_id = genre.genre_id;

 #SAME USING INNER JOIN
 select book.title, genre.name from book inner join genre on book.genre_id = genre.genre_id;


 
 SELECT
 book.title,
 author.name as author,
 publisher.name as publisher
 FROM 
 book
 JOIN author using(author_id)
 JOIN publisher using(publisher);

-- title, author, publisher, genre, price where author ends in king
SELECT
book.title,
author.name as AuthorName,
publisher.name as PublisherName,
genre.name as Genre,
book.price
FROM book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
JOIN genre USING (genre_id)
WHERE author.name LIKE "%KING";

-- title, author name, author country if the user is from USA or CANADA. result should be in order by country
SELECT
book.title,
author.name as AuthorName,
author.country as AuthorCountry
FROM book
JOIN author USING (author_id)
WHERE author.country = "USA" OR author.country = "CANADA"
ORDER BY author.country ASC;

-- second way
SELECT
book.title,
author.name as AuthorName,
author.country as AuthorCountry
FROM book
JOIN author USING (author_id)
WHERE author.country IN ("USA","CANADA")
ORDER BY author.country ASC;

-- Same above query where author is not from USA or CANADA
SELECT
book.title,
author.name as AuthorName,
author.country as AuthorCountry
FROM book
JOIN author USING (author_id)
WHERE author.country != "USA" AND author.country != "CANADA"
ORDER BY author.country ASC;

-- title, author, publisher, price where books are cheaper than $18
SELECT 
book.title,
author.name as AuthorName,
publisher.name as PublisherName,
book.price
FROM book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
WHERE book.price < 18;

-- same query where price between 10 and 25
SELECT 
book.title,
author.name as AuthorName,
publisher.name as PublisherName,
book.price
FROM book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
WHERE book.price between 10 AND 25;

-- add a publisher to publisher table.
INSERT INTO 
publisher 
(
 publisher_id,
 name,
 city,
 phone
 )
VALUES 
(
 8,
 "Arshdeep",
 "Winnipeg",
 "234-9876"
);

-- add a author to author table.
INSERT INTO 
author 
(
 author_id,
 name,
 country
 )
VALUES 
(
 23,
 "Chetan Baghat",
 "India"
);

-- add book without publisher_id
INSERT INTO
book
(
 book_id,
 title,
 year_published,
 num_pages,
 in_print,
 price,
 author_id,
 format_id,
 genre_id
)
VALUES
(
 15,
 "2 States",
 2006,
 487,
 1,
 4.34,
 23,
 1,
 3
);

-- add book without author_id
INSERT INTO
book
(
 book_id,
 title,
 year_published,
 num_pages,
 in_print,
 price,
 publisher_id,
 format_id,
 genre_id
)
VALUES
(
 16,
 "Revolution 2020",
 2014,
 447,
 1,
 4.34,
 8,
 1,
 3
);

-- add book without genre_id
INSERT INTO
book
(
 book_id,
 title,
 year_published,
 num_pages,
 in_print,
 price,
 author_id,
 publisher_id,
 format_id
)
VALUES
(
 17,
 "3 Point Someone",
 2008,
 427,
 1,
 4.34,
 23,
 8,
 1
);

-- LEFT and RIGHT Joins
SELECT
book.book_id,
book.title,
author.author_id,
author.name as author
FROM book
LEFT JOIN author using (author_id);

SELECT
book.book_id,
book.title,
author.author_id,
author.name as author
FROM book
RIGHT JOIN author using (author_id);

#find book which doesn't have genre
SELECT
book.book_id,
book.title,
genre.genre_id
FROM book
LEFT JOIN genre using (genre_id);

-- Finding NULL values in dtabase
# finding book with no author.
 select * from book where author_id is NULL;

-- cross join example
SELECT
book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM 
book,genre;

SELECT
book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM 
book
CROSS JOIN genre;

SELECT
book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM 
book
JOIN genre;

-- all the above is going to dispaly same.

-- Calculations in SQL
update book set price = price * 1.1;

select book_id,title,price as cost, format(price*1.4,2) as cost_price from book;

-- recreating the whole catalog table from bookstore
SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format
FROM
book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
JOIN format USING (format_id)
JOIN genre USING (genre_id);