

SELECT
DISTINCT author.name AS author
FROM 
book 
JOIN
author USING (author_id);

SELECT
book.title,
book.num_pages,
book.price,
author.name AS author,
publisher.name AS publisher
FROM
book
JOIN 
author USING(author_id)
JOIN
publisher USING(publisher_id)
LIMIT 5,5;

REPLACE INTO
book
(book_id,title)
VALUES
(3,'A NEW BOOK');

select min(price) as 'Minimum Price', max(price) as 'Maximum Price', avg(price) as 'Average Price' from book;

SELECT
publisher.name,
count(book.book_id) as 'num_pages'
FROM
book
JOIN publisher using (publisher_id)
group by publisher.name;

SELECT
publisher.name as 'publisher name',
author.name as 'Author name',
count(book.book_id) as 'num_pages'
FROM
book
JOIN publisher using (publisher_id)
JOIN author using(author_id)
group by publisher.name,author.name;


CREATE VIEW book_list as 
select
book.book_id,
book.title,
book.price,
author.name as 'author name',
publisher.name as ' publisher',
genre.name as 'genre',
format.name as 'format'
from book
join author using (author_id)
join publisher using(publisher_id)
join genre using(genre_id)
join format using(format_id);
