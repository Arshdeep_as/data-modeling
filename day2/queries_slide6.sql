select title,year_published,author,publisher,genre,format,in_print from catalog;
select title,author,genre from catalog where author = 'Stephen King';
select title,author,genre from catalog where author = 'Stephen King' or author = 'Frank Herbert';
select title,author,genre from catalog where price < 10;
select title,author,genre,price from catalog where price between 8.0 and 12.0;
select author,author_country from catalog where author_country = 'Canada' or author_country = 'UK';
select author,author_country from catalog where author_country <> 'USA';
select author,author_country from catalog where not author_country = 'USA';
select title, author, publisher, year_published from catalog where year_published between 2000 and 2006;
select title, author, publisher, year_published from catalog where year_published between 2000 and 2006 and in_print = 1 ;
select title, author, publisher, year_published,in_print from catalog where year_published between 2000 and 2006 and in_print = 1 ;
select title, author, publisher, year_published,in_print from catalog where year_published between 2000 and 2006 and in_print = 1 and author_country = 'Canada';
select title, author, publisher, year_published,in_print,author_country from catalog where year_published between 2000 and 2006 and in_print = 1 and author_country = 'Cana
da';