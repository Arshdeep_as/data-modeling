DROP DATABASE IF EXISTS `books`;

CREATE DATABASE `books`;

USE books;

DROP TABLE IF EXISTS `books`;

CREATE TABLE
books
(
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	title VARCHAR(255),
	author VARCHAR(255),
	num_pages INT(11),
	year_published INT(11),
	in_print BOOLEAN
);

INSERT INTO 
books 
(title, author, num_pages, year_published, in_print) 
VALUES 
('Dune', "Frank Herbert", 567, 1975, 1),
('I, Robot', "Isaac Asimov", 275, 1969, 1),
('The Stars My Destination', "Alfred Bester", 209, 1971, 0),
('Cities in Flight', "James Blish", 976, 1972, 1),
('Dahlgren', "Samuel R. Delaney", 846, 1977, 1);