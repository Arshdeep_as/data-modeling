-- MySQL dump 10.16  Distrib 10.2.9-MariaDB, for osx10.12 (x86_64)
--
-- Host: localhost    Database: books
-- ------------------------------------------------------
-- Server version	10.2.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `year_published` int(11) NOT NULL,
  `num_pages` int(11) NOT NULL,
  `in_print` tinyint(1) NOT NULL,
  `author` varchar(255) NOT NULL,
  `author_country` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `publisher_phone` varchar(255) NOT NULL,
  `publisher_city` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `format` varchar(255) NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'Dune',1975,556,1,'Frank Herbert','USA','Ballantine','775-1234','New York','SF',5.99,'Paper','2013-02-10 14:59:47'),(2,'Island',2002,345,1,'Richard Laymon','USA','Dell','766-1313','New York','Horror',4.99,'Paper','2013-02-10 14:59:47'),(3,'A Day in the Life',2012,704,1,'Carmen Ynez','Canada','Penguin','445-0987','Toronto','Literature',22.99,'Hardcover','2013-02-10 14:59:47'),(4,'Under the Dome',2010,1200,1,'Stephen King','USA','Ballantine','775-1234','New York','Horror',17.99,'Trade Paper','2013-02-10 14:59:47'),(5,'Carpet Baggers',1977,340,0,'Lee Sheldon','USA','Putnam','234-8876','New York','Drama',3.99,'Paper','2013-02-10 14:59:47'),(6,'Not a Penny More',1980,300,1,'Daniel Chambers','England','Delacorte','555-1212','London','Politics',5.99,'Paper','2013-02-10 14:59:47'),(7,'A Mixed Blessing',2002,450,1,'Sally Unger','Africa','Sun Press','654-1234','johannesburg','Politics',12.99,'Trade Paper','2013-02-10 14:59:47'),(8,'The Oath',2008,500,1,'John Lescroart','USA','Dell','766-1313','New York','Legal ',24.99,'Hardcover','2013-02-10 14:59:47'),(9,'Carrie',1975,300,0,'Stephen King','USA','Ballantine','775-1234','New York','Horror',4.99,'Paper','2013-02-10 14:59:47'),(10,'Flash Forward',2006,417,1,'Robert J. Sawyer','Canada','DAW','543-1234','New York','SF',19.99,'Trade Paper','2013-02-10 14:59:47');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-11  6:42:00
